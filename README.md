# Board support package for Traverse Layerscape boards

This package installs a couple of scripts to improve the user experience on
Traverse Layerscape boards.

Currently this builds a debian package, it is intended to extend this to other
distributions soonish.

## List of additions:
### SFP+ Enable GPIO handler
Why: Optical SFPs need their TXENABLE pin toggled to enable the laser.

These should be managed by phylink - but the current phylink implementation for DPAA2
doesn't quite support our specific hardware (e.g XFI interface) yet - additionally any
support might take a while to reach downstream kernels.

Will be removed when: phylink support implemented DPAA2 XFI/10G interfaces, and widely shipped.

### SFP+ LED handler
Why: SFP LEDs need to be managed by the system, unlike the copper ports there is
no hardware that can manage these.
Additionally, it is currently not possible to setup netdev LED triggers from device tree.

Will be removed when: When [this patch or similar](https://lore.kernel.org/patchwork/cover/1050791/) is merged.

Note: netdev LED triggers have only been in the mainline kernel since v4.16.

## Distribution specific notes
### openSuSE
The default network management daemon in openSuSE, `wicked` does not support pre-up/post-down scripts by default.

You will need to add `PRE_UP_SCRIPT` and `POST_DOWN_SCRIPT` to the `ifcfg(5)` for the interface, like this:
```
# cat /etc/sysconfig/network/ifcfg-eth8
...
STARTMODE='auto'
PRE_UP_SCRIPT="wicked:/lib/traverse/ten64-sfp-enable-handler"
POST_DOWN_SCRIPT="wicked:/lib/traverse/ten64-sfp-enable-handler"
```
