install:
	install -d $(DESTDIR)/lib/traverse
	install -m 0755 ./network/ten64-sfp-enable-handler $(DESTDIR)/lib/traverse/ten64-sfp-enable-handler
	install -m 0755 ./led/ten64-sfp-led-handler $(DESTDIR)/lib/traverse/ten64-sfp-led-handler
