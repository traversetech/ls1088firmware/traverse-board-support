#!/bin/sh
set -e
OPENSUSE_VERSION=${OPENSUSE_VERSION:-15.1}
CONTAINER_TAG="driverbuild-${OPENSUSE_VERSION}"
docker build -t "${CONTAINER_TAG}" -f rpm/Dockerfile.build --build-arg "version=${OPENSUSE_VERSION}" .
CONTAINER_NAME=$(docker create -w /tmp/build -e COPYRPMS=1 --entrypoint "/tmp/build/rpm/buildrpm.sh" "${CONTAINER_TAG}")
docker start -a "${CONTAINER_NAME}"
docker cp "${CONTAINER_NAME}:/tmp/output" .
docker rm "${CONTAINER_NAME}"
