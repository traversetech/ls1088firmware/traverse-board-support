#norootforbuild

Name:			traverse-board-support
BuildRequires: systemd-rpm-macros
License:		GPL
Group:			System/Kernel
Summary:		Scripts to improve hardware integration on Traverse boards
Version:		0.9.20200608
Release:		0
Source0:		%name-%version.tar.bz2
BuildRoot:		%{_tmppath}/%{name}-%{version}-build
ExclusiveArch:		aarch64


%description
This package contains scripts to implement hardware functions on Traverse boards
that either are not implemented in the kernel.
%prep
%setup
set -- *
mkdir source
mv "$@" source/
cp source/debian/traverse-board-support.ten64-sfp-led-handler.service source/network
mkdir obj

%build


%install
pwd
ls .
make -C $PWD/source DESTDIR=%{buildroot} install

install -d -m 755 %{buildroot}/%{_unitdir}
cp source/network/traverse-board-support.ten64-sfp-led-handler.service %{buildroot}/%{_unitdir}/ten64-sfp-led-handler.service

%post
%systemd_post ten64-sfp-led-handler.service

%preun
%systemd_preun ten64-sfp-led-handler.service

%postun
%systemd_postun_with_restart ten64-sfp-led-handler.service

%files
/lib/traverse/*
%{_unitdir}/ten64-sfp-led-handler.service
%changelog
* Mon Jun 08 2020 - matt@traverse.com.au
- Initial package for RPM based distributions
