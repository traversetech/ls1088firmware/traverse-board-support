#!/bin/sh
set -e

PACKAGE_VERSION=$(cat rpm/traverse-board-support.spec  | grep Version | awk '{print $2}')
mkdir -p "${HOME}/rpmbuild/SOURCES"

SOURCES_FOLDER="/tmp/traverse-board-support-${PACKAGE_VERSION}"
SOURCES_TARBALL="${SOURCES_FOLDER}.tar.bz2"
rm -rf "${SOURCES_FOLDER}"
mkdir -p "${SOURCES_FOLDER}"
cp -r * "${SOURCES_FOLDER}"
tar -C /tmp -Jcvf "${SOURCES_TARBALL}" "traverse-board-support-${PACKAGE_VERSION}"
cp "${SOURCES_FOLDER}.tar.bz2" "${HOME}/rpmbuild/SOURCES"

rpmbuild -ba rpm/traverse-board-support.spec

if [ "${COPYRPMS}" = "1" ]; then
	mkdir -p /tmp/output
	cp ${HOME}/rpmbuild/RPMS/aarch64/* /tmp/output
	cp ${HOME}/rpmbuild/SRPMS/* /tmp/output
	cp "${SOURCES_TARBALL}" /tmp/output
fi
